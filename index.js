const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const busboy = require('connect-busboy');
const fs = require('fs');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser({uploadDir:'./public'}));
app.use(busboy());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    res.send('<h1>Welcome to backend for MãeTerra Hack  -  Filhos da Terra Team =D </h1>');
});
app.post('/upload', function (req, res) {
    var fstream;
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        console.log("Uploading: " + filename);
        //Path where image will be uploaded
        fstream = fs.createWriteStream(__dirname + '/public/' + filename);
        file.pipe(fstream);
        fstream.on('close', function () {
            console.log("Upload Finished of " + filename);
            // sendFileToAI();
            res.send('file has been uploaded!');
        });
    });

    // get the temporary location of the file
 app.get('/download', function(req, res){
     console.log('here')
        const filename = req.query.filename;
        const file = fs.readFileSync(__dirname + '/public/'+filename, 'binary');

        res.setHeader('Content-Length', file.length);
        res.write(file, 'binary');
        res.end();
    });
});
function sendFileToAI(){
    // Replace <Subscription Key> with your valid subscription key.
    const subscriptionKey = '9d56b24c76d14e74863ec61a83f7defd';

// You must use the same location in your REST call as you used to get your
// subscription keys. For example, if you got your subscription keys from
// westus, replace "westcentralus" in the URL below with "westus".
    const uriBase ='https://westcentralus.api.cognitive.microsoft.com/vision/v2.0/ocr';

    const imageUrl = 'http://fundacaocafu.org.br/site/assets/images/cupomFiscal.png';

// Request parameters.
    const params = {
        'language': 'unk',
        'detectOrientation': 'true',
    };

    const options = {
        uri: uriBase,
        qs: params,
        body: '{"url": ' + '"' + imageUrl + '"}',
        headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key' : subscriptionKey
        }
    };

    request.post(options, (error, response, body) => {
        if (error) {
            console.log('Error: ', error);
            return;
        }
        let jsonResponse = JSON.stringify(JSON.parse(body), null, '  ');
        console.log('JSON Response\n');
        console.log(jsonResponse);
    });
}
const port = process.env.PORT || 1337;
app.listen(port);

console.log("Server running at http://localhost:%d", port);
